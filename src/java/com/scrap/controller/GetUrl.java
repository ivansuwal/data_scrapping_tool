
package com.scrap.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class GetUrl extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        Pattern title_pattern = Pattern.compile("<h1 class=\"title\" itemprop=\"name\">(.*?)</h1>");
        Pattern author_pattern=Pattern.compile("<div><strong>Author:(.*?)</div>");
        Pattern language_pattern=Pattern.compile("<div><strong>Language:(.*?)</div>");
        Pattern length_pattern=Pattern.compile("<div>\\s*<strong>Length:(.*?)</div>");
        Pattern publisher_pattern=Pattern.compile("<div>\\s*<strong>Publisher:(.*?)</div>");
        Pattern isbn10_pattern=Pattern.compile("<td class=\"specsKey\">\\s*ISBN-10</td>(.*?)</td>");
        Pattern isbn13_pattern=Pattern.compile("<td class=\"specsKey\">\\s*ISBN-13</td>(.*?)</td>");
        Pattern image_pattern=Pattern.compile("<div class=\"productImages\" data-ctrl=\"ProductImagesController\">(.*?)data-src(.*?)</div>");
        
        
        String url=request.getParameter("url");
        try (PrintWriter out = response.getWriter()) {
             BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new URL(url).openStream()));
            String s;
            StringBuilder builder = new StringBuilder();
            while ((s = bufferedReader.readLine()) != null) {
                builder.append(s);
             }
            Matcher tagmatch = title_pattern.matcher(builder.toString());
            tagmatch.find();
            out.println(tagmatch.group() );
            tagmatch=author_pattern.matcher(builder.toString());
            tagmatch.find();
            out.println(tagmatch.group());
            tagmatch=language_pattern.matcher(builder.toString());
            tagmatch.find();
            out.println(tagmatch.group());
            tagmatch=length_pattern.matcher(builder.toString());
            tagmatch.find();
            out.println(tagmatch.group());
            tagmatch=publisher_pattern.matcher(builder.toString());
            tagmatch.find();
            out.println(tagmatch.group());
            tagmatch=isbn10_pattern.matcher(builder.toString());
            tagmatch.find();
            out.println(tagmatch.group());
            tagmatch=isbn13_pattern.matcher(builder.toString());
            tagmatch.find();
            out.println(tagmatch.group());
            tagmatch=image_pattern.matcher(builder.toString());
            tagmatch.find();
            String link = tagmatch.group().replaceFirst("<div class=\"productImages\" data-ctrl=\"ProductImagesController\">(.*?)data-src=\"", "").replaceFirst("\"\\s*.*","");
            out.println("<img src='"+link+"'/>");
            
            }
    }
}
